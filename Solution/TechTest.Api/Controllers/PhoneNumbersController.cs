﻿using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;

using TechTest.Data;
using TechTest.Data.Models;

namespace TechTest.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class PhoneNumbersController : ControllerBase
	{
		#region Private Members

		private MockData mockData;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PhoneNumbersController"/> class.
		/// </summary>
		public PhoneNumbersController()
		{
			mockData = new MockData();
		}

		#endregion

		#region HTTP Methods

		// GET api/phonenumbers
		[HttpGet]
		public ActionResult All()
		{
			IEnumerable<PhoneNumber> phoneNumbers = mockData.PhoneNumbers;
			return Ok(phoneNumbers);
		}

		// GET api/phonenumbers/1
		[HttpGet("{customerId}")]
		public ActionResult Get(int customerId)
		{
			if (customerId < 1)
			{
				return BadRequest(string.Format("Invalid customerId: {0}", customerId));
			}

			Customer customer = mockData.Customers.FirstOrDefault(x => x.Id == customerId);

			if (customer == null)
			{
				return BadRequest(string.Format("Customer {0} does not exist", customerId));
			}

			IEnumerable<PhoneNumber> customerPhoneNumbers = mockData.PhoneNumbers.Where(x => x.CustomerId == customerId);

			return Ok(customerPhoneNumbers);
		}

		// PUT api/phonenumbers
		[HttpPut("{phoneNumber}")]
		public ActionResult Activate(string phoneNumber)
		{
			if (string.IsNullOrEmpty(phoneNumber) || string.IsNullOrWhiteSpace(phoneNumber))
			{
				return BadRequest(string.Format("Please provide a phone number in order to activate"));
			}

			PhoneNumber number = mockData.PhoneNumbers.FirstOrDefault(x => x.Number == phoneNumber);

			if (number == null)
			{
				return BadRequest(string.Format("The number {0} does not exist", phoneNumber));
			}

			if (number.IsActivated)
			{
				return BadRequest(string.Format("The number {0} is already activated", phoneNumber));
			}

			number.IsActivated = true;
			//mockData.SaveChanges();

			return Ok(number);
		}

		#endregion
	}
}
