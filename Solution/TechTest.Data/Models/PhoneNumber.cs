﻿using System.ComponentModel.DataAnnotations;

namespace TechTest.Data.Models
{
	public class PhoneNumber
	{
		/// <summary>
		/// Gets or sets the number.
		/// </summary>
		/// <value>
		/// The number.
		/// </value>
		[Key]
		[Required]
		public string Number { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this number is activated.
		/// </summary>
		/// <value>
		///   <c>true</c> if this number is activated; otherwise, <c>false</c>.
		/// </value>
		public bool IsActivated { get; set; }

		/// <summary>
		/// Gets or sets the customer identifier.
		/// </summary>
		/// <value>
		/// The customer identifier.
		/// </value>
		[Required]
		public int CustomerId { get; set; }
	}
}
