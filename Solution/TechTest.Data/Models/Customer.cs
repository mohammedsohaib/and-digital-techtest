﻿using System.ComponentModel.DataAnnotations;

namespace TechTest.Data.Models
{
	public class Customer
	{
		/// <summary>
		/// Gets or sets the customer identifier.
		/// </summary>
		/// <value>
		/// The customer identifier.
		/// </value>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[Required]
		public string Name { get; set; }
	}
}