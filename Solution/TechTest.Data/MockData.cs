﻿using Newtonsoft.Json;

using System.Collections.Generic;
using System.IO;
using System.Reflection;

using TechTest.Data.Models;

namespace TechTest.Data
{
	public class MockData
	{
		#region Public Members

		public List<Customer> Customers;
		public List<PhoneNumber> PhoneNumbers;

		#endregion

		#region Private Members

		private string path;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MockData"/> class.
		/// </summary>
		/// <exception cref="InvalidDataException"></exception>
		public MockData()
		{
			GetPath();

			if (File.Exists(path) && Path.GetExtension(path).ToLower() == ".json")
			{
				InitialiseMockData();
			}
			else
			{
				throw new FileNotFoundException();
			}
		}

		#endregion

		#region Public Methods

		public void SaveChanges()
		{
			using (TextWriter file = File.CreateText(path))
			{
				try
				{
					Data data = new Data() { Customers = Customers, PhoneNumbers = PhoneNumbers };
					JsonSerializer serializer = new JsonSerializer();
					serializer.Serialize(file, data);
				}
				catch (JsonWriterException ex)
				{
					throw ex;
				}
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Gets the path for the mock data file.
		/// </summary>
		private void GetPath()
		{
			string assemblyLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			path = Path.Combine(assemblyLocation, @"MockData.json");
		}

		/// <summary>
		/// Initialises the mock data.
		/// </summary>
		private void InitialiseMockData()
		{
			using (StreamReader file = File.OpenText(path))
			{
				try
				{
					JsonSerializer serializer = new JsonSerializer();
					Data data = (Data)serializer.Deserialize(file, typeof(Data));

					Customers = data.Customers;
					PhoneNumbers = data.PhoneNumbers;			
				}
				catch(JsonReaderException ex)
				{
					throw ex;
				}
			}
		}

		#endregion

		#region Nested Types

		public class Data
		{
			public List<Customer> Customers;
			public List<PhoneNumber> PhoneNumbers;

			public Data()
			{
				Customers = new List<Customer>();
				PhoneNumbers = new List<PhoneNumber>();
			}
		}

		#endregion
	}
}