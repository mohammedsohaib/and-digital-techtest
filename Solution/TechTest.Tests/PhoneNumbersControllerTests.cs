using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;

using TechTest.Api.Controllers;
using TechTest.Data.Models;

using Xunit;

namespace TechTest.Tests
{
	public class PhoneNumbersControllerTests
	{
		#region GetAll()

		[Fact]
		public void GetAll_CheckIfAllAreReturned()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResult = controller.All();
			Assert.NotNull(actionResult);

			OkObjectResult result = actionResult as OkObjectResult;
			Assert.NotNull(result);
			Assert.Equal(StatusCodes.Status200OK, result.StatusCode);

			List<PhoneNumber> phoneNumbers = result.Value as List<PhoneNumber>;
			Assert.Equal(7, phoneNumbers.Count);
		}

		#endregion

		#region Get(int customerId)

		[Fact]
		public void Get_CheckInvalidCustomerId()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResult = controller.Get(0);
			Assert.NotNull(actionResult);

			BadRequestObjectResult result = actionResult as BadRequestObjectResult;
			Assert.NotNull(result);

			Assert.Equal(StatusCodes.Status400BadRequest, result.StatusCode);
		}

		[Fact]
		public void Get_CheckNonExistentCustomerId()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResult = controller.Get(7);
			Assert.NotNull(actionResult);

			BadRequestObjectResult result = actionResult as BadRequestObjectResult;
			Assert.NotNull(result);
			Assert.Equal(StatusCodes.Status400BadRequest, result.StatusCode);
		}

		[Fact]
		public void Get_CheckIfCorrectItemsAreReturned()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResult = controller.Get(2);
			Assert.NotNull(actionResult);

			OkObjectResult result = actionResult as OkObjectResult;
			Assert.NotNull(result);
			Assert.Equal(StatusCodes.Status200OK, result.StatusCode);

			IEnumerable<PhoneNumber> phoneNumbers = result.Value as IEnumerable<PhoneNumber>;
			bool checkList = phoneNumbers.Any(x => x.CustomerId != 2);

			Assert.False(checkList);
			Assert.Equal(2, phoneNumbers.Count());
		}

		#endregion

		#region Activate(string phoneNumber)

		[Fact]
		public void Activate_ShouldNotAcceptEmptyString()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResult = controller.Activate(string.Empty);
			Assert.NotNull(actionResult);

			BadRequestObjectResult result = actionResult as BadRequestObjectResult;
			Assert.NotNull(result);
			Assert.Equal(StatusCodes.Status400BadRequest, result.StatusCode);
		}

		[Fact]
		public void Activate_ShouldNotAcceptWhiteSpaceString()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResult = controller.Activate(" ");
			Assert.NotNull(actionResult);

			BadRequestObjectResult result = actionResult as BadRequestObjectResult;
			Assert.NotNull(result);
			Assert.Equal(StatusCodes.Status400BadRequest, result.StatusCode);
		}

		[Fact]
		public void Activate_ShouldNotAcceptNullString()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResult = controller.Activate(null);
			Assert.NotNull(actionResult);

			BadRequestObjectResult result = actionResult as BadRequestObjectResult;
			Assert.NotNull(result);
			Assert.Equal(StatusCodes.Status400BadRequest, result.StatusCode);
		}

		[Fact]
		public void Activate_CheckForNonExistentNumber()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResult = controller.Activate("07123456789");
			Assert.NotNull(actionResult);

			BadRequestObjectResult result = actionResult as BadRequestObjectResult;
			Assert.NotNull(result);
			Assert.Equal(StatusCodes.Status400BadRequest, result.StatusCode);
		}

		[Fact]
		public void Activate_CheckForAlreadyActivatedNumber()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResult = controller.Activate("07574529846");
			Assert.NotNull(actionResult);

			BadRequestObjectResult result = actionResult as BadRequestObjectResult;
			Assert.NotNull(result);
			Assert.Equal(StatusCodes.Status400BadRequest, result.StatusCode);
		}

		[Fact]
		public void Activate_CheckIfNumberActivates()
		{
			PhoneNumbersController controller = new PhoneNumbersController();

			ActionResult actionResultOne = controller.Get(3);
			Assert.NotNull(actionResultOne);

			OkObjectResult resultOne = actionResultOne as OkObjectResult;
			Assert.NotNull(resultOne);
			Assert.Equal(StatusCodes.Status200OK, resultOne.StatusCode);

			IEnumerable<PhoneNumber> phoneNumbers = resultOne.Value as IEnumerable<PhoneNumber>;
			PhoneNumber phoneNumber = phoneNumbers.FirstOrDefault(x => x.Number == "07578525845");
			Assert.NotNull(phoneNumber);
			Assert.False(phoneNumber.IsActivated);

			ActionResult actionResultTwo = controller.Activate(phoneNumber.Number);
			Assert.NotNull(actionResultTwo);

			OkObjectResult resultTwo = actionResultTwo as OkObjectResult;
			Assert.NotNull(resultTwo);
			Assert.Equal(StatusCodes.Status200OK, resultTwo.StatusCode);

			PhoneNumber phoneNumberActivated = resultTwo.Value as PhoneNumber;
			Assert.NotNull(phoneNumberActivated);
			Assert.True(phoneNumberActivated.IsActivated);
		}

		#endregion
	}
}
